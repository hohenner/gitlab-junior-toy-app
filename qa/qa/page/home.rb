module QA
  module Page
    class Home < Base
      include QA::Page::Components::Navbar

      def click_sign_up_link
        click_element(:sign_up_link)
      end

      def click_new_top_level_group_link
        click_element(:new_top_level_group_link)
      end
    end
  end
end

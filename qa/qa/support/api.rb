require 'httparty'

module QA
  module Support
    module API
      extend self

      HTTP_STATUS_OK = 200
      HTTP_STATUS_CREATED = 201
      HTTP_STATUS_NO_CONTENT = 204
      HTTP_STATUS_ACCEPTED = 202
      HTTP_STATUS_NOT_FOUND = 404
      HTTP_STATUS_SERVER_ERROR = 500

      def post(url, payload)
        HTTParty.post(url, body: payload.to_json, headers: { 'Content-Type' => 'application/json' })
      end

      def get(url)
          HTTParty.get(url)
      end


      def delete(url)
          HTTParty.delete(url)
      end


      def parse_body(response)
        JSON.parse(response.body, symbolize_names: true)
      end

      def return_response_or_raise(error)
        raise error, masked_url(error.to_s) unless error.respond_to?(:response) && error.response

        error.response
      end
    end
  end
end

require 'rails_helper'

RSpec.describe UsersHelper, type: :helper do
  describe '#gravatar_for' do
    let(:user) { create(:user) }

    it 'returns an image tag with gravatar url' do
      allow(Digest::MD5).to receive(:hexdigest).and_return('randomhex')
      expect(gravatar_for(user)).to eq("<img alt=\"#{user.name}\" class=\"gravatar\" src=\"https://secure.gravatar.com/avatar/randomhex?s=80\" />")
    end
  end
end

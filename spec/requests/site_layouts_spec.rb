require 'rails_helper'

RSpec.describe 'SiteLayouts', type: :request do
  describe 'GET /' do
    it 'has correct path in links' do
      get root_path
      expect(response.body).to include(help_path)
      expect(response.body).to include(about_path)
      expect(response.body).to include(contact_path)
    end
  end
end

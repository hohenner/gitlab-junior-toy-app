require 'rails_helper'

RSpec.describe 'UsersLogins', type: :request do

  let(:user) { create(:user) }

  it 'does not login with invalid information' do
    get login_path
    expect(response).to have_http_status(:success)
    post login_path, params: { session: { email: "", password: "" } }
    expect(response).to have_http_status(:unprocessable_entity)
    expect(flash).not_to be_empty
    expect(is_logged_in?).to be_falsey

    get root_path
    expect(flash).to be_empty
  end

  it 'logs in with valid information followed by logout' do
    log_in_as(user)
    expect(response).to redirect_to(root_url)
    expect(is_logged_in?).to be_truthy

    delete logout_path
    assert_response :see_other
    expect(response).to redirect_to(root_url)
    expect(is_logged_in?).to be_falsey
  end

  it 'sets remember_token when remember_me is set' do
    log_in_as(user, remember_me: '1')
    expect(cookies[:remember_token]).not_to be_blank
  end

  it 'removes remember_token token when remember_me is not set ' do
    log_in_as(user, remember_me: '1')
    log_in_as(user, remember_me: '0')
    expect(cookies[:remember_token]).to be_blank
  end
end


def log_in_as(user, password: 'password', remember_me: '1')
  post login_path, params: { session: { email: user.email,
    password: password,
    remember_me: remember_me } }
end

def is_logged_in?
  !session[:user_id].nil?
end

require 'rails_helper'

RSpec.describe 'Users Edits', type: :request do
  let(:user) { create(:user) }
  let(:other_user) { create(:user) }

  context 'when logged in' do
    before do
      log_in_as(user)
    end

    it 'it does not save with invalid parameters' do
      get edit_user_path(user)
      expect(response).to have_http_status(200)

      patch user_path(user), params: { user: { name: '',
        email: 'foo@invalid',
        password: 'foo',
        password_confirmation: 'bar' } }

      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'saves successfully with valid parameters' do
      get edit_user_path(user)
      name = 'Foo Bar'
      email = 'foo@bar.com'
      patch user_path(user), params: { user: { name: name,
        email: email,
        password: "",
        password_confirmation: "" } }

      expect(flash).not_to be_empty
      expect(response).to redirect_to(user)

      user.reload
      assert_equal name, user.name
      assert_equal email, user.email
    end
  end

  context 'when not logged in' do
    it 'redirects to login url on edit' do
      get edit_user_path(user)
      expect(flash).not_to be_empty
      expect(response).to redirect_to(login_url)
    end

    it 'redirects to login url on update' do
      patch user_path(user), params: { user: { name: user.name,
        email: user.email } }
      expect(flash).not_to be_empty
      expect(response).to redirect_to(login_url)
    end

    it 'redirects to edit page after login' do
      get edit_user_path(user)
      log_in_as(user)
      expect(response).to redirect_to(edit_user_url(user))
    end
  end

  context 'when logged in as other user' do
    before do
      log_in_as(other_user)
    end

    it 'redirects to root url on edit' do
      get edit_user_path(user)
      expect(response).to redirect_to(root_url)
    end

    it 'redirects to root url on update' do
      patch user_path(user), params: { user: { name: user.name,
        email: user.email } }
      expect(response).to redirect_to(root_url)
    end

    it 'does not allow the admin attribute to be edited via the web' do
      expect(other_user).not_to be_admin

      patch user_path(other_user), params: {
        user: { password: 'password',
          password_confirmation: 'password',
          admin: true } }

      expect(other_user).not_to be_admin
    end
  end

  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
      password: password,
      remember_me: remember_me } }
  end
end

require 'rails_helper'

RSpec.describe "PersonalAccessTokens", type: :request do
  let(:user) { create(:user) }
  let!(:personal_access_tokens) { create_list(:personal_access_token, 30, user: user) }

  describe 'GET /index' do
    it 'redirects to index when not logged in' do
      get personal_access_tokens_path
      expect(response).to redirect_to(login_url)
    end

    it 'paginates tokens' do
      log_in_as(user)
      get personal_access_tokens_path
      PersonalAccessToken.paginate(page: 1).each do |token|
        expect(response.body).to include("<h4 class=\"name\">#{token.name}</h4>")
      end
    end
  end

  describe 'POST /personal_access_tokens' do
    let(:invalid_post_params) do
      { personal_access_token: { name: '', value: 'abc123', user_id: user.id } }
    end

    let(:valid_post_params) do
      { personal_access_token: { name: 'New PAT', value: 'abc123abc123abc123', user_id: user.id  } }
    end

    before do
      log_in_as(user)
    end

    it 'does not save with invalid params' do
      expect { post personal_access_tokens_path, params: invalid_post_params }.to_not change { PersonalAccessToken.count }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'saves with valid params' do
      expect { post personal_access_tokens_path, params: valid_post_params }.to change { PersonalAccessToken.count }
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(personal_access_tokens_url)
    end
  end
end

def log_in_as(user, password: 'password', remember_me: '1')
  post login_path, params: { session: { email: user.email,
    password: password,
    remember_me: remember_me } }
end

require 'rails_helper'

RSpec.describe API::V1::Groups, type: :api do
  include Rack::Test::Methods

  def app
    API::V1::Groups
  end

  describe 'GET /groups' do
    let!(:group) { create(:group) }
    let!(:groups) { create_list(:group, 30) }

    it 'returns all groups' do
      get '/api/v1/groups'

      response_body = JSON.parse(last_response.body)
      expect(last_response.status).to eq(200)
      expect(response_body.count).to eq 31
      expect(response_body.first['name']).to eq(group.name)
      expect(response_body.first['description']).to eq(group.description)
    end
  end

  describe 'GET /groups/:id' do
    let!(:group) { create(:group) }

    it 'returns all groups' do
      get "/api/v1/groups/#{group.id}"

      response_body = JSON.parse(last_response.body)
      expect(last_response.status).to eq(200)
      expect(response_body['name']).to eq(group.name)
      expect(response_body['description']).to eq(group.description)
    end
  end

  describe 'GET /groups/:id/subgroups' do
    let(:user) { create(:user) }

    let!(:parent_group) { create(:group, owner: user) }
    let!(:sub_group) { create(:group, :with_parent, parent_group:parent_group, owner: user ) }


    it 'returns all groups' do
      get "/api/v1/groups/#{parent_group.id}/subgroups"

      response_body = JSON.parse(last_response.body)
      expect(last_response.status).to eq(200)
      expect(response_body.first['name']).to eq(sub_group.name)
      expect(response_body.first['description']).to eq(sub_group.description)
      expect(response_body.first['parent_group_id']).to eq(parent_group.id)
    end
  end

  describe 'DELETE /groups/:id' do
    let!(:owner_user) { create(:user) }
    let(:owner_personal_access_token) { create(:personal_access_token, user: owner_user).value }
    let!(:other_user) { create(:user) }
    let(:other_user_personal_access_token) { create(:personal_access_token, user: other_user).value }
    let!(:group) { create(:group, owner: owner_user) }
    let!(:admin) { create(:user, :admin) }
    let(:admin_personal_access_token) { create(:personal_access_token, user: admin).value }

    it 'fails without a personal access token' do
      delete "/api/v1/groups/#{group.id}"

      expect(last_response.status).to eq(401)
    end

    it 'fails with a personal access token on incorrect user' do
      delete "/api/v1/groups/#{group.id}", private_token: other_user_personal_access_token

      expect(last_response.status).to eq(401)
    end

    it 'succeeds with a personal access token to the owner user' do
      delete "/api/v1/groups/#{group.id}", private_token: owner_personal_access_token

      expect(last_response.status).to eq(204)
    end

    it 'succeeds with a personal access token to the admin user' do
      delete "/api/v1/groups/#{group.id}", private_token: owner_personal_access_token

      expect(last_response.status).to eq(204)
    end

    it 'returns all groups' do
      delete "/api/v1/groups/#{group.id}", private_token: admin_personal_access_token

      expect(last_response.status).to eq(204)
    end
  end

  describe 'POST /groups' do
    let(:user_personal_access_token) { create(:personal_access_token).value }
    let(:post_params) do
      { name: 'Group Name',
        description: 'Group Description'
      }
    end

    it 'requires a personal access token' do
      post '/api/v1/groups', post_params
      JSON.parse(last_response.body)
      expect(last_response.status).to eq(401)
    end

    it 'succeeds with an access token' do
      post '/api/v1/groups', post_params.merge(private_token: user_personal_access_token)
      expect(last_response.status).to eq(201)
    end
  end
end

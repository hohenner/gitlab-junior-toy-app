FactoryBot.define do
  factory :personal_access_token do
    name { FFaker::Lorem.word }
    value { PersonalAccessToken.generate }
    user
  end
end

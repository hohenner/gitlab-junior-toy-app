class CreateGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.string :description
      t.references :parent_group, foreign_key: { to_table: :groups }
      t.references :owner, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
